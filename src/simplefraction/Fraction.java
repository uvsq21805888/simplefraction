package simplefraction;

public class Fraction {
	private int denum, nume; 
	
	public Fraction (int d,int r) {
		
		this.denum=d; 
		this.nume=r;
	}

	public int getDenum() {
		return denum;
	}

	public void setDenum(int denum) {
		this.denum = denum;
	}

	public int getNume() {
		return nume;
	}

	public void setNume(int nume) {
		this.nume = nume;
	}
	@Override
	public String toString() {
		return "la fraction est:"+getNume()+"/"+getDenum()+"="+nume/denum ;
	}
	
	
	
}

